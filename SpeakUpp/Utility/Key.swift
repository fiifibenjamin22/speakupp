//
//  Key.swift
//  Korba
//
//  Created by Bright Ahedor on 16/12/2017.
//  Copyright © 2017 Halges Limited. All rights reserved.
//

import Foundation

struct Key {
    static let primaryHexCode = "#429AD2"
    static let primaryHomeHexCode = "#7974b7"
    static let secondaryHexCode = "#63656a"
    static let tertiaryHexCode = "#898B8E"
    static let backgroundColor = "#E8E8E8"
    static let startedButtonColor = "#32A7D8"
 
}

